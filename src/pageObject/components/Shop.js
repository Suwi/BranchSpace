import Component from './Component';

class Shop extends Component {
    constructor(selector, blockSelector) {
        super();
        this.elementSel = selector;
        this.productBlockSel = `${selector} ${blockSelector}`;
    }

    get productBlocksNumber() {
        const elements = browser.elements(this.productBlockSel);
        return elements.value.length;
    }
}

export default Shop;