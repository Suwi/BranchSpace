class Component {
    constructor() {
        this.elementSel = null;
    }

    waitForElement(time = 5000) {
        browser.waitForVisible(this.elementSel, time);
    }

    get isVisible() {
        const result = browser.isVisible(this.elementSel);
        return result;
    }

    waitForInvisible(time = 5000) {
        browser.waitUntil(() => this.isVisible === false, time);
        return this;
    }
}

export default Component;