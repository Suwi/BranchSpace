import Component from './Component';

class Cart extends Component {
    constructor(selector) {
        super();
        this.elementSel = selector;
        this.productsSel = `${this.elementSel} .cart__list--products--logo`;
        this.continueSel = `${this.elementSel} .button--continue`;
        this.continueReadySel = `${this.elementSel} .button--continue.ready`;
        this.errorSel = '.cart--errors';
    }

    get productNumber() {
        const elements = browser.elements(this.productsSel);
        return elements.value.length;
    }

    get isButtonVisible() {
        return browser.isVisible(this.continueSel);
    }

    get isButtonActive() {
        return browser.isVisible(this.continueReadySel);
    }

    get isErrorVisible() {
        return browser.isVisible(this.errorSel);
    }
}

export default Cart;