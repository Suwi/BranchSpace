import Component from './Component';
import { selectors } from '../../utils/const';

class ProductTile extends Component {
    constructor(name) {
        super();
        this.name = name;
        this.elementSel = `div[data-product='${name}']`;
    }

    click() {
        this.waitForElement();
        //toDo: remove static wait when quick adding bug will be fixed
        browser.pause(1000);
        browser.click(this.elementSel);
    }

    get isVisibleInCart() {
        return browser.isVisible(`${selectors.cart.selector} ${this.elementSel}`);
    }

    get isVisibleInShop() {
        return browser.isVisible(`${selectors.shop.selector} ${this.elementSel}`);
    }

    waitForInvisibleInCart(time = 5000) {
        browser.waitUntil(() => this.isVisibleInCart === false, time);
        return this;
    }

    waitForInvisibleInShop(time = 5000) {
        browser.waitUntil(() => this.isVisibleInShop === false, time);
        return this;
    }
}

export default ProductTile;