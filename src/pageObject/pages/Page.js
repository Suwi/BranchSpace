/*eslint class-methods-use-this: "warn" */

import { selectors } from '../../utils/const';
import ProductTile from '../components/ProductTile';
import Cart from '../components/Cart';
import Shop from '../components/Shop';

class Page {
    constructor() {
        this.shop = new Shop(selectors.shop.selector, selectors.shop.blockSelector);
        this.cart = new Cart(selectors.cart.selector);
        this.jiraSoftware = new ProductTile(selectors.jiraSoftware.name);
        this.jiraServiceDesk = new ProductTile(selectors.jiraServiceDesk.name);
        this.jiraCore = new ProductTile(selectors.jiraCore.name);
        this.bitBucket = new ProductTile(selectors.bitBucket.name);
        this.bamboo = new ProductTile(selectors.bamboo.name);
        this.fisheye = new ProductTile(selectors.fisheye.name);
    }

    open(url = '') {
        browser.url(url);
    }
}

const page = new Page();

export { page };
export default Page;