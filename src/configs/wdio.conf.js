exports.config = {
    specs: [
        './src/specs/*',
    ],
    exclude: [
    ],
    maxInstances: 10,
    capabilities: [{
        maxInstances: 5,
        browserName: 'chrome',
    }],
    services: ['selenium-standalone'],
    seleniumLogs: './logs',
    sync: true,
    logLevel: 'silent',
    deprecationWarnings: true,
    bail: 0,
    screenshotPath: './errorShots/',
    waitforTimeout: 10000,
    baseUrl: 'https://www.atlassian.com/',
    framework: 'mocha',
    mochaOpts: {
        compilers: ['js:babel-register'],
        ui: 'bdd',
        timeout: 60000,
    },
    reporters: ['dot', 'allure'],
    reporterOptions: {
        allure: {
            outputDir: 'allure-results',
        },
    },
};
