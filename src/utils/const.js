export const selectors = {
    cart: {
        selector: '.component--buy-cart',
    },
    shop: {
        selector: '.component--buy-products__list--products.active',
        blockSelector: '.component--buy-products__block--product-group',
    },
    jiraSoftware: {
        name: 'jira-software',
    },
    jiraServiceDesk: {
        name: 'jira-servicedesk',
    },
    jiraCore: {
        name: 'jira-core',
    },
    fisheye: {
        name: 'fisheye',
    },
    bitBucket: {
        name: 'bitbucket',
    },
    bamboo: {
        name: 'bamboo',
    },
    cookieAgreeButton: {
        selector: '.optanon-alert-box-button.optanon-button-allow',
    },
};