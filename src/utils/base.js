export { page } from '../pageObject/pages/Page';
export { selectors } from './const';
export { closeCookieMessage } from './utility';