import { page, closeCookieMessage } from '../utils/base';

const chai = require('chai');

global.expect = chai.expect;

describe('Tests for atlassian buy page', () => {
    beforeEach('', () => {
        page.open('/buy');
        page.jiraSoftware.waitForElement();
    });
    it('add item to cart', () => {
        closeCookieMessage();
        expect(page.jiraSoftware.isVisibleInShop, `product ${page.jiraSoftware.name} is not visible in shop`)
            .to.equal(true);
        page.jiraSoftware.click();
        page.jiraSoftware.waitForInvisibleInShop();
        expect(page.jiraSoftware.isVisibleInCart, `product ${page.jiraSoftware.name} is not visible in cart`)
            .to.equal(true);
        expect(page.jiraSoftware.isVisibleInShop, `product ${page.jiraSoftware.name} is visible in shop`)
            .to.equal(false);
    });

    it('remove item from cart', () => {
        page.jiraSoftware.click();
        page.jiraSoftware.waitForInvisibleInShop();
        expect(page.jiraSoftware.isVisibleInCart, `product ${page.jiraSoftware.name} is not visible in cart`)
            .to.equal(true);
        page.jiraSoftware.click();
        page.jiraSoftware.waitForInvisibleInCart();
        expect(page.jiraSoftware.isVisibleInShop, `product ${page.jiraSoftware.name} is not visible in shop`)
            .to.equal(true);
        expect(page.jiraSoftware.isVisibleInCart, `product ${page.jiraSoftware.name} is visible in cart`)
            .to.equal(false);
    });

    it('block disappears when has 0 products', () => {
        const expectedBlocksNumber = page.shop.productBlocksNumber - 1;
        const errorMessage = 'number of product blocks is different than expected';
        page.jiraSoftware.click();
        page.jiraServiceDesk.click();
        page.jiraCore.click();
        page.jiraCore.waitForInvisibleInShop();
        expect(page.shop.productBlocksNumber, errorMessage).to.equal(expectedBlocksNumber);
    });

    it('continue button is initially disabled', () => {
        expect(page.cart.isButtonVisible).to.equal(true);
        expect(page.cart.isButtonActive).to.equal(false);
    });

    it('continue button is enabled with one product added', () => {
        page.jiraSoftware.click();
        browser.pause(1000); //temporary solution
        expect(page.cart.isButtonVisible).to.equal(true);
        expect(page.cart.isButtonActive).to.equal(true);
    });

    //bug with many instance of one product in cart
    it('continue button is disabled again when all products are removed', () => {
        page.jiraSoftware.click();
        page.jiraSoftware.click();
        browser.pause(1000); //temporary solution
        expect(page.cart.isButtonVisible).to.equal(true);
        expect(page.cart.isButtonActive).to.equal(false);
    });

    it('only five server products can be added to the cart', () => {
        //toDo create utils method
        page.jiraSoftware.click();
        page.jiraServiceDesk.click();
        page.jiraCore.click();
        page.bitBucket.click();
        page.bamboo.click();
        page.fisheye.click();
        expect(page.cart.isErrorVisible).to.equal(true);
        expect(page.cart.productNumber).to.equal(5);
    });

    //toDo
    it('only four cloud products can be added to the cart');
    it('switch between product types');
    it('cart items are not removed while switching between product types');
});